%
% tick.pl
%
% Eduardo Munoz - Prolog tick (University of Cambridge assignment)
% January 2012
%
% Program to solve foam puzzles in the shape of cubes by interlocking the
% "fingers" that are on each edge of the sides.
%
% This code was written to be compiled using SWI-Prolog
%
% The main predicate in this file is puzzle/2. Some examples of use:
% - Finding the solutions to a puzzle (note the bindings produced have been
% omitted. Each solution is made up of 6 lines, line n describes the piece and
% its orientation to go into slot n of the cube (as described below).
%          _____
%          | 0 |
%      ____|___|____
%      | 2 | 1 | 3 |
%      |___|___|___|
%          | 4 |
%          |___|
%          | 5 |
%          |___|
%
%
% (you can achieve an equivalent query by just typing)
% ?- test2.
%
% ?- puzzle([['74',[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]],
%            ['65',[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]],
%	     ['13',[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]],
%	     ['Cc',[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]],
%	     ['98',[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]],
%	     ['02',[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]]],
%            S).
%
% [74,[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]] at 0
% [65,[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]] at 2
% [98,[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]] at 3
% [13,[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]] at 1
% [Cc,[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]] at -4
% [02,[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]] at 2 ;
%
% [74,[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]] at 0
% [65,[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]] at 2
% [98,[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]] at 3
% [13,[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]] at 1
% [Cc,[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]] at 0
% [02,[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]] at 2;
%
% [74,[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]] at 0
% [65,[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]] at 2
% [98,[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]] at 3
% [13,[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]] at 1
% [02,[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]] at 3
% [Cc,[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]] at -3 ;
%
% [74,[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]] at 0
% [65,[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]] at 2
% [98,[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]] at 3
% [13,[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]] at 1
% [02,[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]] at 3
% [Cc,[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]] at 3
%
%
% - Checking a solution is correct. Note that for optimization reasons, the
% program doesn't determine isomorphic solutions. It only ever considers
% solutions in which the first specified piece is in the slot 0 when
% assemblying the cube.
%



% Load the libraries files
:- use_module(library(lists)).
:- use_module(library(apply)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Supporting predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% piece(?P) - Succeeds if P is a piece of a specific version of the puzzle
piece(P) :-
	P=['74', [[1,1,0,0,1,0], [0,1,0,1,0,0], [0,1,0,0,1,0], [0,1,0,0,1,1]]];
	P=['65', [[1,1,0,0,1,1], [1,0,1,1,0,0], [0,0,1,1,0,0], [0,1,0,1,0,1]]];
	P=['13', [[0,1,0,1,0,1], [1,1,0,1,0,1], [1,1,0,0,1,1], [1,1,0,0,1,0]]];
	P=['Cc', [[0,0,1,1,0,0], [0,0,1,1,0,0], [0,1,0,0,1,0], [0,0,1,1,0,0]]];
	P=['98', [[1,1,0,0,1,0], [0,1,0,0,1,0], [0,0,1,1,0,0], [0,0,1,1,0,1]]];
	P=['02', [[0,0,1,1,0,0], [0,1,0,0,1,1], [1,0,1,1,0,0], [0,0,1,1,0,0]]].

% testing piece
:- bagof(P, piece(P),Ps), length(Ps,L), L=6.


% rotate(?L,?R) - Rotate a list by one place using append
rotate([A|As],B) :- append(As,[A],B).


% rotate(?A,+N,?B) - Rotate a list by n places
rotate(A,N,B) :- N>0,!, M is N-1, rotate(A,C), rotate(C,M,B),!.
rotate(A,0,A).


% xor(?A,?B) - Exclusive-or using 1's and 0's as inputs
xor(1,0).
xor(0,1).


% only_one(?A,?B,?C) - Check that only one argument is 1 while the others are 0
only_one(0,0,1).
only_one(0,1,0).
only_one(1,0,0).


% range(+Min,+Max,-Val) - Unify the Val with Min in the first evaluation and
% then all all values up to Max -1
range(A,A,B) :- B is A-1.
range(Min, Max, Val) :- Min<Max, Val=Min.
range(Min,Max,Val) :- L is Min+1, L<Max, range(L,Max,Val).


% flipped(+P,?FP) - Succeeds if FP is the flipped version of piece P
flipped([Name,[S0,S1,S2,S3]], [Name,[R0,R1,R2,R3]]) :-
	maplist(reverse, [S0,S1,S2,S3], [R0,R3,R2,R1]).


orientation(P,N,RP) :- N < 0,!, M is -N, flipped(P,FP), orientation(FP,M,RP).
orientation([Name,Bits],N,[Name,RBits]) :- N > 0,!, rotate(Bits,N,RBits).
orientation(P,0,P).

% compatible(+P1,+Side1,+P2,+Side2) - Succeeds if the Side1 of P1 can be
% plugged into Side2 of P2
compatible([_Name1,Edges1],Side1,[_Name2,Edges2],Side2) :-
	nth0(Side1,Edges1,[_A0,A1,A2,A3,A4,_A5]),
	nth0(Side2,Edges2,[_B0,B1,B2,B3,B4,_B5]),
	maplist(xor,[A1,A2,A3,A4],[B4,B3,B2,B1]),!.


% compatible_corner(+P1,+Side1,+P2,+Side2,+P3,+Side3) - Succeeds if there is
% exactly one finger in the first position of each side of each piece
compatible_corner([_Name1,Edges1],Side1,[_Name2,Edges2],Side2,[_Name3,Edges3],Side3) :-
	nth0(Side1,Edges1,E1), nth0(0,E1,F1),
	nth0(Side2,Edges2,E2), nth0(0,E2,F2),
	nth0(Side3,Edges3,E3), nth0(0,E3,F3),
	only_one(F1,F2,F3).


del(X,[X|Tail],Tail).
del(X,[Y|Tail],[Y|Tail1]) :-
	del(X,Tail,Tail1).

insert(X,List,BiggerList) :-
	del(X,BiggerList,List).


% perm(+P,?PP) - Succeeds if PP is a permutation of the list P
perm([],[]).
perm([X|L],P) :-
	perm(L,L1),
	insert(X,L1,P).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main predicate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% puzzle(+Ps,?S) - Succeeds if S contains solutions to the puzzle specified by
% the pieces Ps
puzzle([P0|Ps],S) :-

	perm(Ps,[P1,P2,P3,P4,P5]),

	O0=0,
	orientation(P0,O0,OP0),

	% generate the orientations
	% range(-4,4,O0),
	range(-4,4,O1),
	range(-4,4,O2),
	range(-4,4,O3),
	range(-4,4,O4),
	range(-4,4,O5),	

	% generate the oriented pieces
	% orientation(P0,O0,OP0),
	orientation(P1,O1,OP1),
	orientation(P2,O2,OP2),
	orientation(P3,O3,OP3),
	orientation(P4,O4,OP4),
	orientation(P5,O5,OP5),

	% check compatibility of edges
	compatible(OP0,2,OP1,0),
	compatible(OP0,3,OP2,0),
	compatible(OP1,3,OP2,1),
	compatible(OP0,1,OP3,0),
	compatible(OP1,1,OP3,3),
	compatible(OP1,2,OP4,0),
	compatible(OP2,2,OP4,3),
	compatible(OP3,2,OP4,1),
	compatible(OP4,2,OP5,0),
	compatible(OP2,3,OP5,3),
	compatible(OP0,0,OP5,2),
	compatible(OP3,1,OP5,1),

	% check compatibility of corners
	compatible_corner(OP0,3,OP1,0,OP2,1),
	compatible_corner(OP0,2,OP1,1,OP3,0),
	compatible_corner(OP2,2,OP1,3,OP4,0),
	compatible_corner(OP3,3,OP1,2,OP4,1),
	compatible_corner(OP5,0,OP4,3,OP2,3),
	compatible_corner(OP5,1,OP4,2,OP3,2),
	compatible_corner(OP5,2,OP0,1,OP3,1),
	compatible_corner(OP5,3,OP0,0,OP2,0),

	% make unification
	S = [[P0,O0],[P1,O1],[P2,O2],[P3,O3],[P4,O4],[P5,O5]],

	% pretty print the solutions
	format('~w at ~w~n', [P0,O0]),
	format('~w at ~w~n', [P1,O1]),
	format('~w at ~w~n', [P2,O2]),
	format('~w at ~w~n', [P3,O3]),
	format('~w at ~w~n', [P4,O4]),
	format('~w at ~w~n', [P5,O5]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create(Ps) :-
	bagof(P,piece(P),Ps).

test2 :-
	create(Ps),
	puzzle(Ps,S).

% test_number(?L) - Succeeds if L is the number of solutions found by puzzle/2
test_number(L) :-
	create(Ps),
	bagof(S,puzzle(Ps,S),Ss),
	length(Ss,L).


% test - Test provided by the specification
test :-
	P0 = ['74',[[1,1,0,0,1,0],[0,1,0,1,0,0],[0,1,0,0,1,0],[0,1,0,0,1,1]]],
	P1 = ['65',[[1,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,1,0,1]]],
	P2 = ['13',[[0,1,0,1,0,1],[1,1,0,1,0,1],[1,1,0,0,1,1],[1,1,0,0,1,0]]],
	P3 = ['Cc',[[0,0,1,1,0,0],[0,0,1,1,0,0],[0,1,0,0,1,0],[0,0,1,1,0,0]]],
	P4 = ['98',[[1,1,0,0,1,0],[0,1,0,0,1,0],[0,0,1,1,0,0],[0,0,1,1,0,1]]],
	P5 = ['02',[[0,0,1,1,0,0],[0,1,0,0,1,1],[1,0,1,1,0,0],[0,0,1,1,0,0]]],
	puzzle([P0,P1, P2, P3, P4,P5], S),
	(
		S = [[P0,0],[P1,2],[P4,3],[P2,1],[P3,0], [P5, 2]];
		S = [[P0,0],[P1,2],[P4,3],[P2,1],[P3,-4], [P5, 2]];
		S = [[P0,0],[P1,2],[P4,3],[P2,1],[P5,3], [P3, 3]];
		S = [[P0,0],[P1,2],[P4,3],[P2,1],[P5,3], [P3, -3]]
	).
